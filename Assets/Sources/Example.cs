﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum QualitySettingType
{ 
    High,
    Mid,
    Low
}

public class Example : MonoBehaviour
{
    [SerializeField] RectTransform rtMenuTop, rtMenuBottom;
    [SerializeField] Image imgBg;
    [SerializeField] Text info;

    private WebViewObject target;
    bool isOpened;
    public static Vector2 screenSize { get; private set; } = new Vector2(720.0f, 1280.0f);
    
    [RuntimeInitializeOnLoadMethod]
    void Awake()
    {
        // 해상도 세팅
        float screenWidth = Screen.width;
        float screenHegiht = Screen.height;
        Vector2 curSize = new Vector2(screenWidth, screenHegiht);
        float curRatio = curSize.y / curSize.x;

        float targetWidth = 720.0f;

        if (curRatio < 1.7f)
            targetWidth = 1280.0f * (curSize.x / curSize.y);

        curSize.x = targetWidth;
        curSize.y = targetWidth * curRatio;

        screenSize = curSize;

        //SetResolution(QualitySettingType.High);
    }

    void Start()
    {
        StartCoroutine(CoInvokeChangeColor());
    }

    IEnumerator CoInvokeChangeColor()
    {
        while (true)
        {
            var color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 0.5f);
            imgBg.color = color;
            yield return new WaitForSeconds(1f);
        }
    }

    public void OnClick_SetResolution(int type)
    {
        SetResolution((QualitySettingType)type);
    }

    public void OnClick_Show()
    {
        float height = Screen.height;

        if (!isOpened)
            OpenUrl("https://www.google.com/");
        else
            CloseWebview();
    }

    public void OpenUrl(string url)
    {
        isOpened = true;
        Open(url);
    }
    public static void SetResolution(QualitySettingType type)
    {
        float factor = 1.0f;
        switch (type)
        {
            case QualitySettingType.High:
                factor = 1.0f;
                break;
            case QualitySettingType.Mid:
                factor = 0.75f;
                break;
            case QualitySettingType.Low:
                factor = 0.5f;
                break;
        }
        Vector2 targetSize = screenSize * factor;
        Screen.SetResolution((int)targetSize.x, (int)targetSize.y, true);
    }

    private void InitWebview()
    {
        // 웹뷰 닫았다 열 때 마다 마진 설정이 변경되는 이슈가 있어서 수정함
        if (target) Destroy(target);

        target = gameObject.AddComponent<WebViewObject>();
        target.Init(enableWKWebView: true);

        float uiHeight = 105f;
        float systemHeight = Display.main.systemHeight;
        int spaceCount = (int)(systemHeight / uiHeight);
        Debug.Log(spaceCount);
        info.text = $"width:{Screen.width} height:{Screen.height} Native Width:{Display.main.systemWidth} Native Height:{Display.main.systemHeight} Rendering Width:{Display.main.renderingWidth} Rendering Height:{Display.main.renderingHeight}";



        target.SetMargins(0, 150, 0, 150);
    }

    
    private void Open(string url)
    {
        InitWebview();
        try
        {
            target.SetVisibility(true);
            target.LoadURL(url);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    public void CloseWebview()
    {
        isOpened = false;
        target?.SetVisibility(false);
    }

    public void OnClick_Forward()
    {
        if (target.CanGoForward())
            target.GoForward();
    }

    public void OnClick_Back()
    {
        if (target.CanGoBack())
            target.GoBack();
    }
}
