﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerSetter : MonoBehaviour
{
    public CanvasScaler scaler;

    private void OnValidate()
    {
        if (scaler == null)
            scaler = GetComponentInChildren<CanvasScaler>();
    }

    private void Start()
    {
        if (scaler == null)
            return;

        scaler.referenceResolution = Example.screenSize;
    }
}
